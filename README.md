## SonarQube + GitLab - Sample Security Project

Sample project via Ruby on Rails template for SonarQube CE scanning in GitLab CI.

### How To Use This Project

Scans should already be configured in the pipeline via:

- `sonar-project.properties`: contains project properties for SonarQube
- `SONAR_HOST_URL`: External IP for GCP `docker` VM
  - this value may need updating, verify before executing scan and stop VM
    after use
- `SONAR_TOKEN`: Access token from SonarQube
- `DAST_WEBSITE` is currently set to scan the `SONAR_HOST_URL`
  - obv be careful with this...

# CHANGEBOX

```shell
To trigger MRs, easier to make changes here.
```

## Original Template Information Below

### Ruby on Rails template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/rails).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all dependencies pre-installed and Rails server will open a web preview.
